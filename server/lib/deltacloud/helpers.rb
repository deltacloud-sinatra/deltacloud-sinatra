require 'deltacloud/helpers/application_helper'
require 'deltacloud/helpers/url_helper'
require 'deltacloud/helpers/driver_helper'
require 'deltacloud/helpers/hardware_profiles_helper'

helpers ApplicationHelper, URLHelper, DriverHelper, HardwareProfilesHelper
